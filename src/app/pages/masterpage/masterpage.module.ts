import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MasterpageRoutingModule } from './masterpage-routing.module';
import { HomepageComponent } from './homepage/homepage.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivityDetailComponent } from './activity-detail/activity-detail.component';
import { CreateActivityComponent } from './create-activity/create-activity.component';
import { CreateDialogComponent } from './create-dialog/create-dialog.component';
import {MatTabsModule} from '@angular/material/tabs';

@NgModule({
  declarations: [HomepageComponent, ActivityDetailComponent, CreateActivityComponent, CreateDialogComponent],
  imports: [
    CommonModule,
    MasterpageRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class MasterpageModule { }
